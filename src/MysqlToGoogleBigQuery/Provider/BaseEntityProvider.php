<?php
namespace MysqlToGoogleBigQuery\Provider;

use Doctrine\ORM\EntityManager;
use MysqlToGoogleBigQuery\Exception\LoggedException;
use MysqlToGoogleBigQuery\Request\ColumnRequestList;

abstract class BaseEntityProvider
{
    /**
     * @var EntityManager
     */
    private $em = null;

    /**
     * @var ColumnRequestList
     */
    private $columnRequestList = null;

    /**
     * @param EntityManager $em
     * @param ColumnRequestList $columnRequestList
     */
    public function __construct(EntityManager $em, ColumnRequestList $columnRequestList)
    {
        $this->em = $em;
        $this->columnRequestList = $columnRequestList;
    }

    /**
     * @param $query
     * @return true
     * @throws LoggedException
     */
    protected function validateCustomSql($query)
    {
        $errorMessage = "Error in the '{$this->getDbTable()}' table.";
        if (strpos($query, 'ORDER BY') !== false) {
            throw new LoggedException($errorMessage . " The query can't contain 'ORDER BY construction!'");
        }

        if (strpos($query, 'LIMIT') !== false) {
            throw new LoggedException($errorMessage . " The query can't contain 'LIMIT construction!'");
        }

        if (strpos($query, 'OFFSET') !== false) {
            throw new LoggedException($errorMessage . " The query can't contain 'OFFSET construction!'");
        }

        if (strpos($query, '%s') === false) {
            throw new LoggedException($errorMessage . " The query have to contain '%s' construction instead of columns!'");
        }

        return true;
    }

    /**
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @return ColumnRequestList
     */
    protected function getColumnRequestList()
    {
        return $this->columnRequestList;
    }

    /**
     * @return string
     */
    protected function getDbTable()
    {
        return $this->columnRequestList->getDbTableName();
    }

}