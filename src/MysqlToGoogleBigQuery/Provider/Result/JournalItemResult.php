<?php
namespace MysqlToGoogleBigQuery\Provider\Result;


class JournalItemResult
{
    private $items = array();

    /**
     * JournalItemResult constructor.
     *
     * @param array $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * @param $dbTable
     *
     * @return int
     */
    public function getLastId($dbTable)
    {
        foreach ($this->items as $item) {
            if ($item->tableName == $dbTable) {

                return $item->lastPk;
            }
        }

        return 0;
    }

    /**
     * @param $dbTable
     *
     * @return int
     */
    public function getFirstId($dbTable)
    {
        foreach ($this->items as $item) {
            if ($item->tableName == $dbTable) {

                return $item->firstPk;
            }
        }

        return 0;
    }


}