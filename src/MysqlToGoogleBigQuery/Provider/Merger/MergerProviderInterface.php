<?php
namespace MysqlToGoogleBigQuery\Provider\Merger;

use MysqlToGoogleBigQuery\Exception\LoggedException;
use MysqlToGoogleBigQuery\Provider\Result\IterationResult;
use MysqlToGoogleBigQuery\Provider\Result\TableValueResult;

interface MergerProviderInterface
{
    /**
     * @param $lastId
     * @return IterationResult
     * @throws LoggedException
     */
    public function getIterationRowCount($lastId);

    /**
     * @param $lastId
     * @param $limit
     * @return TableValueResult
     * @throws LoggedException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getRows($lastId, $limit);

}