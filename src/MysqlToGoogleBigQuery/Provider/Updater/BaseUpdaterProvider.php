<?php
namespace MysqlToGoogleBigQuery\Provider\Updater;

use Doctrine\ORM\EntityManager;
use MysqlToGoogleBigQuery\Provider\BaseEntityProvider;
use MysqlToGoogleBigQuery\Request\ColumnRequestList;
use MysqlToGoogleBigQuery\Service\Api\ApiServiceInterface;

class BaseUpdaterProvider extends BaseEntityProvider
{
    /**
     * @var ApiServiceInterface
     */
    private $api = null;

    /**
     * @param EntityManager $em
     * @param ColumnRequestList $columnRequestList
     * @param ApiServiceInterface $api
     */
    public function __construct(EntityManager $em, ColumnRequestList $columnRequestList, ApiServiceInterface $api)
    {
        parent::__construct($em, $columnRequestList);
        $this->api = $api;
    }

    /**
     * @param $sql
     * @param $firstId
     * @param $lastId
     * @return string
     */
    protected function addExcludeIdsCondition($sql, $firstId, $lastId)
    {
        $excludeIds = $this->getApi()->getRowIds($this->getColumnRequestList()->getTableName(), $firstId, $lastId);
        if (count($excludeIds) == 0) {

            return $sql;
        }

        return $sql . sprintf(
            " AND `%s`.`id` NOT IN (" . implode(",", $excludeIds) . ")",
            $this->getColumnRequestList()->getDbTableName()
        );
    }

    /**
     * @return ApiServiceInterface
     */
    public function getApi()
    {
        return $this->api;
    }

}