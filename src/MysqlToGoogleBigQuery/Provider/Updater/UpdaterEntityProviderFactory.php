<?php
namespace MysqlToGoogleBigQuery\Provider\Updater;

use Doctrine\ORM\EntityManager;
use MysqlToGoogleBigQuery\Request\ColumnRequestList;
use MysqlToGoogleBigQuery\Service\Api\ApiServiceInterface;

class UpdaterEntityProviderFactory
{
    /**
     * @param EntityManager $em
     * @param ColumnRequestList $columnRequestList
     * @param ApiServiceInterface $api
     * @return CustomUpdaterProvider|DefaultUpdaterProvider
     */
    public static function create(EntityManager $em, ColumnRequestList $columnRequestList, ApiServiceInterface $api)
    {
        return empty($columnRequestList->getQuery())
            ? new DefaultUpdaterProvider($em, $columnRequestList, $api)
            : new CustomUpdaterProvider($em, $columnRequestList, $api);
    }

}