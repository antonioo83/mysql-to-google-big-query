<?php
namespace MysqlToGoogleBigQuery\Provider\Updater;

use MysqlToGoogleBigQuery\Exception\LoggedException;
use MysqlToGoogleBigQuery\Provider\Result\TableValueResult;

interface UpdaterProviderInterface
{
    /**
     * @param $firstId
     * @param $lastId
     * @return array|float|int|string
     */
    public function getIterationRowCount($firstId, $lastId);

    /**
     * @param $lastId
     * @param $firstId
     * @return TableValueResult
     * @throws LoggedException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getRows($firstId, $lastId);

}