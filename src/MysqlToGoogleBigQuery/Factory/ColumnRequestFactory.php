<?php
namespace MysqlToGoogleBigQuery\Factory;

use MysqlToGoogleBigQuery\Component\Config\ColumnConfig;
use MysqlToGoogleBigQuery\Component\Config\DataTypeConfigList;
use MysqlToGoogleBigQuery\Exception\LoggedException;
use MysqlToGoogleBigQuery\Request\ColumnRequest;
use MysqlToGoogleBigQuery\Request\DbColumnRequest;

class ColumnRequestFactory extends BaseFactory
{
    /**
     * @param array $column
     * @param ColumnConfig $configService
     * @param DataTypeConfigList $dataTypeList
     * @return ColumnRequest
     * @throws LoggedException
     */
    public static function create(array $column, ColumnConfig $configService, DataTypeConfigList $dataTypeList)
    {
        $dbColumn = new DbColumnRequest(
            $column['COLUMN_NAME'],
            $column['DATA_TYPE'],
            $column['IS_NULLABLE'],
            $column['CHARACTER_MAXIMUM_LENGTH']
        );
        return new ColumnRequest(
            $configService->getStorageName($dbColumn->getName()),
            $dataTypeList->getStorageDataType($dbColumn->getType()),
            $configService->getStorageMode($dbColumn->getNullable()),
            $configService->getStorageDescription($dbColumn->getDescription()),
            $dbColumn
        );
    }

}