<?php
namespace MysqlToGoogleBigQuery\Factory;

use MysqlToGoogleBigQuery\Service\Api\BigQueryApiServiceFactory;
use MysqlToGoogleBigQuery\Service\StorageService;

class StorageServiceFactory extends BaseFactory
{
    /**
     * @param $config
     * @return StorageService
     */
    public static function createByConfig($config)
    {
        $api = BigQueryApiServiceFactory::create(
            $config['mysqlToGoogleBigQuery']['projectId'],
            $config['mysqlToGoogleBigQuery']['keyFilePath'],
            $config['mysqlToGoogleBigQuery']['dataSet']
        );

        return new StorageService($api);
    }



}