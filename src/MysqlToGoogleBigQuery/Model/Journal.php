<?php
namespace MysqlToGoogleBigQuery\Model;

use Doctrine\ORM\Mapping as ORM;
use MysqlToGoogleBigQuery\Model\Type\CommandType;
use MysqlToGoogleBigQuery\Traits\CommonTrait;

/**
 * CourierLocation
 *
 * @ORM\Table(name="mq_journal"))
 * @ORM\Entity
 */
class Journal
{
    use CommonTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="tableName", type="string", length=100, nullable=false)
     */
    public $tableName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="itemCount", type="bigint", nullable=false)
     */
    public $itemCount = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="firstPk", type="bigint", nullable=false)
     */
    public $firstPk = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="lastPk", type="bigint", nullable=false)
     */
    public $lastPk = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="commandType", type="string", length=20, nullable=false)
     */
    public $commandType = CommandType::CREATE;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000, nullable=false)
     */
    public $description = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    public $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="isSuccess", type="boolean", nullable=false)
     */
    public $isSuccess = true;

    /**
     * @var string
     *
     * @ORM\Column(name="errorMessage", type="string", length=1000, nullable=false)
     */
    public $errorMessage = '';

}