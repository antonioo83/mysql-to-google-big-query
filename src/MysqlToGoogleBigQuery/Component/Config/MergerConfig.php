<?php
namespace MysqlToGoogleBigQuery\Component\Config;

class MergerConfig
{
    private $packageSize = 0;

    /**
     * @param int $packageSize
     */
    public function __construct($packageSize)
    {
        $this->packageSize = $packageSize;
    }

    /**
     * @return int
     */
    public function getPackageSize()
    {
        return $this->packageSize;
    }

}