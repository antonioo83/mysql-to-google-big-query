<?php
namespace MysqlToGoogleBigQuery\Component\Config;

use Doctrine\Common\Collections\ArrayCollection;

class ColumnConfigList extends ArrayCollection
{
    /**
     * @param $key
     * @return ColumnConfig|null
     */
    public function get($key)
    {
        return parent::get($key);
    }

}