<?php
namespace MysqlToGoogleBigQuery\Service\Api;

use Google\Cloud\BigQuery\Table;
use MysqlToGoogleBigQuery\Exception\LoggedException;

interface ApiServiceInterface
{
    public function createTable($bigQueryTable, array $columns, $description);

    public function multipleInsert(Table $table, array $data);

    public function getRowIds($storageTableName, $fromId, $toId);

    /**
     * @param $storageTableName
     * @param array $attributes
     * @param $id
     * @return array
     * @throws LoggedException
     */
    public function update($storageTableName, array $attributes, $id);
}